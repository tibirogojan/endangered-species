package com.tibi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.tibi.model.FilterResults;
import com.tibi.model.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        final Injector injector = Guice.createInjector(new ApplicationModule());

        final EndangeredSpeciesFilter filter = injector.getInstance(EndangeredSpeciesFilter.class);

        FilterResults results = filter.filterForEndangeredSpeciesAndMammals();

        final List<Species> criticallyEndangeredSpecies = results.getCriticallyEndangeredSpecies();
        final List<Species> mammals = results.getMammals();

        if (!criticallyEndangeredSpecies.isEmpty()) {
            LOGGER.info("------------------- Listing all {} critically endangered species for the region {} ...", criticallyEndangeredSpecies.size(), results.getRegion().getName());
            criticallyEndangeredSpecies.forEach(s -> LOGGER.info("{}", s));
        } else {
            LOGGER.info("There are no critically endangered species in the region {} to list!", results.getRegion().getName());
        }

        if (!mammals.isEmpty()) {
            LOGGER.info("------------------- Listing all {} mammals for the region {} ...", mammals.size(), results.getRegion().getName());
            mammals.forEach(s -> LOGGER.info("{}", s));
        } else {
            LOGGER.info("There are no mammals in the region {} to list!", results.getRegion().getName());
        }
    }
}
