package com.tibi;

import com.tibi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Class that is able to call the public API of IUCN
 */
@Singleton
public class IUCNRedListClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(IUCNRedListClient.class);

    private final String regionsEndpointTemplate;
    private String speciesByRegionEndpointTemplate;
    private String conservationMeasuresEndpointTemplate;
    private final String token;

    private final JsonApiClient jsonApiClient;

    @Inject
    public IUCNRedListClient(
            @Named("regionsEndpointTemplate") String regionsEndpointTemplate,
            @Named("speciesByRegionTemplate") String speciesByRegionEndpointTemplate,
            @Named("conservationMeasuresEndpointTemplate") String conservationMeasuresEndpointTemplate,
            @Named("apiToken") String token,
            JsonApiClient jsonApiClient) {
        this.regionsEndpointTemplate = regionsEndpointTemplate;
        this.speciesByRegionEndpointTemplate = speciesByRegionEndpointTemplate;
        this.conservationMeasuresEndpointTemplate = conservationMeasuresEndpointTemplate;
        this.token = token;
        this.jsonApiClient = jsonApiClient;

    }

    /**
     * Corresponds to the /api/v3/region/list endpoint
     *
     * @return all the regions present in the IUCN system
     */
    public List<Region> retrieveAllRegions() {
        List<Region> regions = Collections.emptyList();
        try {
            String url = String.format(regionsEndpointTemplate, token);
            final RegionsResult regionsResult = jsonApiClient.callEnpointAndParse(url, RegionsResult.class);
            regions = regionsResult.getResults();
        } catch (IOException | RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return regions;
    }

    /**
     * Corresponds to the /api/v3/species/region/:region_identifier/page/:page_number endpoint
     *
     * @param region     a valid region identifier from the regions endpoint
     * @param pageNumber which page
     * @return all the species corresponding to that region.
     */
    public List<Species> retrieveEndangeredSpecies(Region region, int pageNumber) {
        List<Species> result = Collections.emptyList();
        try {
            String url = String.format(speciesByRegionEndpointTemplate, region.getIdentifier(), pageNumber, token);
            final SpeciesResult speciesResult = jsonApiClient.callEnpointAndParse(url, SpeciesResult.class);
            result = speciesResult.getResult();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Corresponds to the /api/v3/measures/species/id/:id endpoint
     *
     * @param speciesId the taxonid of the species
     * @return all recorder conservation measures for that species
     */
    public List<ConservationMeasure> retrieveConservationMeasuresForSpecies(int speciesId) {
        List<ConservationMeasure> result = Collections.emptyList();
        try {
            String url = String.format(conservationMeasuresEndpointTemplate, speciesId, token);
            final ConservationMeasureResult conservationMeasureResult = jsonApiClient.callEnpointAndParse(url, ConservationMeasureResult.class);
            result = conservationMeasureResult.getResult();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
