package com.tibi;

import com.google.gson.Gson;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;

/**
 * A Http client that after successfully retrieving the JSON content, parses it to a class of choice
 */
@Singleton
public class JsonApiClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonApiClient.class);

    private final Gson gson;
    private final int timeout;

    @Inject
    public JsonApiClient(Gson gson, @Named("connectTimeout") Integer timeout) {
        this.gson = gson;
        this.timeout = timeout;
    }

    /**
     * @param apiEndpoint an url that is expected to respond with a JSON content
     * @param t           a class to which to attempt to parse the JSON to
     * @param <T>
     * @return the parsed result
     * @throws IOException      in case of IO exceptions
     * @throws RuntimeException any runtime exception is cascaded up for the client to have a better option to deal with it
     */
    public <T> T callEnpointAndParse(String apiEndpoint, Class<T> t) throws IOException, RuntimeException {
        LOGGER.debug("Calling API endpoint {}", apiEndpoint);
        final String responsePayload = Request.Get(apiEndpoint)
                .addHeader("Accept", "application/json")
                .connectTimeout(timeout)
                .execute()
                .returnContent().asString();
        return gson.fromJson(responsePayload, t);
    }
}
