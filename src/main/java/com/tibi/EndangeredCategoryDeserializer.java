package com.tibi;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.tibi.model.EndangeredCategory;

import java.lang.reflect.Type;
import java.util.Arrays;

public class EndangeredCategoryDeserializer implements JsonDeserializer<EndangeredCategory> {
    @Override
    public EndangeredCategory deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final String stringValue = json.getAsString();
        final EndangeredCategory endangeredCategory = Arrays.stream(EndangeredCategory.values())
                .filter(cat -> cat.name().equalsIgnoreCase(stringValue))
                .findFirst().orElse(EndangeredCategory.UNKNOWN);
        return endangeredCategory;
    }
}
