package com.tibi;

import com.tibi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Class that holds the logic that retrieves species according to the assignment description
 */
public class EndangeredSpeciesFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(EndangeredSpeciesFilter.class);
    private static final int FIRST_PAGE = 0;

    private final IUCNRedListClient client;
    private final Random random;

    @Inject
    public EndangeredSpeciesFilter(IUCNRedListClient client, Random random) {
        this.client = client;
        this.random = random;
    }

    /**
     * @return a {@link FilterResults} object that has the list of critically endangered species
     * and a second list of mammals for a specific region
     */
    public FilterResults filterForEndangeredSpeciesAndMammals() {
        List<Region> regions = client.retrieveAllRegions();
        final Region selectedRegion = regions.get(random.nextInt(regions.size()));
//        final Region selectedRegion = new Region();
//        selectedRegion.setIdentifier("europe");

        final List<Species> allSpecies = client.retrieveEndangeredSpecies(selectedRegion, FIRST_PAGE);

        LOGGER.debug("Retrieved {} species for region {} ", allSpecies.size(), selectedRegion.getName());
        // first get the mammals so it's not spoiled by the add conservation measures code.
        // I understood that point 6 expects to work on the original values retrieved at step 4 (allSpecies)
        final List<Species> mammals = allSpecies.stream()
                .filter(isMammal())
                .collect(Collectors.toList());

        //TODO calls to conservation measures API are done serially. Change to parallel approach
        final List<Species> criticallyEndangeredSpecies = allSpecies.stream()
                .filter(this::isCriticallyEndangeredSpecies)
                .map(this::addConservationMeasures)
                .collect(Collectors.toList());

        return new FilterResults(criticallyEndangeredSpecies, mammals, selectedRegion);
    }

    private Predicate<Species> isMammal() {
        return species -> species.getBiologicalClass() == BiologicalClass.MAMMALIA;
    }

    private Species addConservationMeasures(Species species) {
        final List<ConservationMeasure> conservationMeasuresList = client.retrieveConservationMeasuresForSpecies(species.getId());
        if (!conservationMeasuresList.isEmpty()) {
            final String titlesSummary = conservationMeasuresList.stream()
                    .map(ConservationMeasure::getTitle)
                    .collect(Collectors.joining(", "));
            species.setConservationMeasuresSummary(titlesSummary);
        }
        return species;
    }

    private boolean isCriticallyEndangeredSpecies(Species s) {
        return s.getEndangeredCategory() == EndangeredCategory.CR;
    }
}
