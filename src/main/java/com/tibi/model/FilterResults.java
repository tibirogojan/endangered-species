package com.tibi.model;

import java.util.List;

public class FilterResults {

    private final List<Species> criticallyEndangeredSpecies;
    private final List<Species> mammals;
    private final Region region;

    public FilterResults(List<Species> criticallyEndangeredSpecies, List<Species> mammals, Region region) {
        this.criticallyEndangeredSpecies = criticallyEndangeredSpecies;
        this.mammals = mammals;
        this.region = region;
    }

    public List<Species> getCriticallyEndangeredSpecies() {
        return criticallyEndangeredSpecies;
    }

    public List<Species> getMammals() {
        return mammals;
    }

    public Region getRegion() {
        return region;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("FilterResults{");
        sb.append("criticallyEndangeredSpecies=").append(criticallyEndangeredSpecies);
        sb.append(", mammals=").append(mammals);
        sb.append(", region=").append(region);
        sb.append('}');
        return sb.toString();
    }
}
