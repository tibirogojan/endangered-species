package com.tibi.model;

import java.util.List;
import java.util.Objects;

public class ConservationMeasureResult {

    private int speciesId;
    private List<ConservationMeasure> result;

    public int getSpeciesId() {
        return speciesId;
    }

    public void setSpeciesId(int speciesId) {
        this.speciesId = speciesId;
    }

    public List<ConservationMeasure> getResult() {
        return result;
    }

    public void setResult(List<ConservationMeasure> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConservationMeasureResult that = (ConservationMeasureResult) o;
        return speciesId == that.speciesId &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(speciesId, result);
    }
}
