package com.tibi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class SpeciesResult {

    private int count;
    @SerializedName("region_identifier")
    private String region;
    private int page;
    private List<Species> result;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Species> getResult() {
        return result;
    }

    public void setResult(List<Species> result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpeciesResult that = (SpeciesResult) o;
        return count == that.count &&
                page == that.page &&
                Objects.equals(region, that.region) &&
                Objects.equals(result, that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, region, page, result);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SpeciesResult{");
        sb.append("count=").append(count);
        sb.append(", region=").append(region);
        sb.append(", page=").append(page);
        sb.append(", result=").append("...");
        sb.append('}');
        return sb.toString();
    }
}
