package com.tibi.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.Optional;

public class Species {

    @SerializedName("taxonid")
    private int id;
    @SerializedName("class_name")
    private BiologicalClass biologicalClass;
    @SerializedName("category")
    private EndangeredCategory endangeredCategory;
    private String conservationMeasuresSummary;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BiologicalClass getBiologicalClass() {
        return biologicalClass;
    }

    public void setBiologicalClass(BiologicalClass biologicalClass) {
        this.biologicalClass = biologicalClass;
    }

    public EndangeredCategory getEndangeredCategory() {
        return endangeredCategory;
    }

    public void setEndangeredCategory(EndangeredCategory endangeredCategory) {
        this.endangeredCategory = endangeredCategory;
    }


    public Optional<String> getConservationMeasuresSummary() {
        return Optional.ofNullable(conservationMeasuresSummary);
    }

    public void setConservationMeasuresSummary(String conservationMeasuresSummary) {
        this.conservationMeasuresSummary = conservationMeasuresSummary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Species species = (Species) o;
        return id == species.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Species{");
        sb.append("id=").append(id);
        sb.append(", biologicalClass=").append(biologicalClass);
        sb.append(", endangeredCategory=").append(endangeredCategory);
        sb.append(", conservationMeasuresSummary='").append(getConservationMeasuresSummary().orElse("NA!")).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
