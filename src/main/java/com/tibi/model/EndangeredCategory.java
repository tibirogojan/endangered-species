package com.tibi.model;

public enum EndangeredCategory {
    EX,
    EW,
    CR,
    EN,
    VU,
    NT,
    LC,
    DD,
    NE,
    UNKNOWN;

}
