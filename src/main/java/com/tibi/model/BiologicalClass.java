package com.tibi.model;

public enum BiologicalClass {
    MAMMALIA,
    GASTROPODA,
    REPTILIA,
    AVES,
    UNKNOWN;

}
