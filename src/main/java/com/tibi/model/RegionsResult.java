package com.tibi.model;

import java.util.List;
import java.util.Objects;

public class RegionsResult {

    private int count;
    private List<Region> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Region> getResults() {
        return results;
    }

    public void setResults(List<Region> results) {
        this.results = results;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionsResult that = (RegionsResult) o;
        return count == that.count &&
                Objects.equals(results, that.results);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, results);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegionsResult{");
        sb.append("count=").append(count);
        sb.append(", results=").append(results);
        sb.append('}');
        return sb.toString();
    }
}
