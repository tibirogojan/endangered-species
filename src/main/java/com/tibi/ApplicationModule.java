package com.tibi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.tibi.model.BiologicalClass;
import com.tibi.model.EndangeredCategory;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Random;

/**
 * Guice module for our app.
 */
public class ApplicationModule extends AbstractModule {

    protected void configure() {

    }

    @Singleton
    @Provides
    public Gson providesGson() {
        return new GsonBuilder()
                .registerTypeAdapter(BiologicalClass.class, new BiologicalClassDeserializer())
                .registerTypeAdapter(EndangeredCategory.class, new EndangeredCategoryDeserializer())
                .create();
    }

    @Named("connectTimeout")
    @Provides
    public Integer providesConnectTimeout() {
        return 60_000;
    }

    @Singleton
    @Provides
    public Random providesRandom() {
        return new Random();
    }

    @Named("regionsEndpointTemplate")
    @Provides
    public String providesRegionsEndpointTemplate() {
        return "http://apiv3.iucnredlist.org/api/v3/region/list?token=%s";
    }

    @Named("speciesByRegionTemplate")
    @Provides
    public String providesSpeciesByRegionTemplate() {
        return "http://apiv3.iucnredlist.org/api/v3/species/region/%s/page/%d?token=%s";
    }

    @Named("conservationMeasuresEndpointTemplate")
    @Provides
    public String providesConservationMeasuresEndpointTemplate() {
        return "http://apiv3.iucnredlist.org/api/v3/measures/species/id/%d?token=%s";
    }


    @Named("apiToken")
    @Provides
    public String providesApiToken() {
        return "9bb4facb6d23f48efbf424bb05c0c1ef1cf6f468393bc745d42179ac4aca5fee";
    }
}
