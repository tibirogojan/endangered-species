package com.tibi;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.tibi.model.BiologicalClass;

import java.lang.reflect.Type;
import java.util.Arrays;

public class BiologicalClassDeserializer implements JsonDeserializer<BiologicalClass> {

    @Override
    public BiologicalClass deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final String stringValue = json.getAsString();
        final BiologicalClass parsedEnum = Arrays.stream(BiologicalClass.values())
                .filter(cat -> cat.name().equalsIgnoreCase(stringValue))
                .findFirst().orElse(BiologicalClass.UNKNOWN);
        return parsedEnum;
    }
}
