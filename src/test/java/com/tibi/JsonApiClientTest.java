package com.tibi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.tibi.model.RegionsResult;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * This unit test demonstrates that some components are not easily unit-testable.
 * We cannot mock Gson, since it's final. So no control on that.
 * Similarly we cannot mock the FluentApi Http Client, since it's not injected.
 * <p>
 * We could do some functional test, where we use some bogus endpoint values that we know we cannot connect to.
 * Or we could use the real endpoint for the happy flow.
 * We could tweak the connect timeout values (ms) to our advantage. Like using 1 ms that will force a ConnectTimeoutException
 * But all of this would make this not a unit test anymore. It won't run consistently, and could behave differently in some environments.
 * Happy flow url might work on your laptop, but fail when you are no longer connected to the internet.
 * CD pipeline might have some firewall rules to prevent some calls, and it will fail there..
 */
public class JsonApiClientTest {

    private JsonApiClient client;
    //Mockito cannot mock final classes, so we must use real thing, or add another level of indirection so that we can mock that
    private Gson gson = new GsonBuilder().create();

    @BeforeMethod
    public void setUp() {
        client = new JsonApiClient(gson, 60_000);
    }

    @Test(expectedExceptions = IOException.class, enabled = false)
    public void shouldCascadeIoExceptionIfThrownByTheHttpClient() throws Exception {
        //CANNOT mock the fluent api client to throw an IoException!
        final RegionsResult regionsResult = client.callEnpointAndParse("http://google.com", RegionsResult.class);
    }

    @Test(expectedExceptions = JsonParseException.class, enabled = false)
    public void shouldThrowExceptionIfJsonParinsgIssuesOccur() {
        //TODO again we cannot force Gson directly to throw one.
    }

    @Test(enabled = false)
    public void shouldCallAndParseCorrectly() throws Exception {
        //TODO Again we cannot control the components to act in isolation as we want.
        final RegionsResult regionsResult = client.callEnpointAndParse("http://google.com", RegionsResult.class);
        Assert.assertTrue(regionsResult != null);
    }
}
