package com.tibi;

import com.tibi.model.Region;
import com.tibi.model.RegionsResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class IUCNRedListClientTest {

    private IUCNRedListClient client;
    private JsonApiClient jsonApiClient;

    @BeforeMethod
    public void setUp() {
        jsonApiClient = mock(JsonApiClient.class);
        client = new IUCNRedListClient("http://iucn.com/regions",
                "http://iucn.com/species/region",
                "http://iucn.com/measures",
                "123abc",
                jsonApiClient);
    }

    @Test
    public void shouldReturnResultWhenCallSuccessful() throws Exception {
        final RegionsResult mockedRegionsResponse = new RegionsResult();
        final List<Region> mockedResult = Arrays.asList(new Region());
        mockedRegionsResponse.setCount(mockedResult.size());
        mockedRegionsResponse.setResults(mockedResult);
        when(jsonApiClient.callEnpointAndParse(anyString(), any())).thenReturn(mockedRegionsResponse);

        final List<Region> actualRegions = client.retrieveAllRegions();
        assertFalse(actualRegions.isEmpty());
        assertEquals(actualRegions, mockedResult);
    }

    @Test
    public void shouldReturnEmptyListWhenIOExceptionIsThrownByClient() throws Exception {
        when(jsonApiClient.callEnpointAndParse(anyString(), any())).thenThrow(new IOException("Could not connect"));

        final List<Region> regions = client.retrieveAllRegions();
        assertTrue(regions.isEmpty());
    }

    @Test
    public void shouldReturnEmptyListWhenRuntimeExceptionIsThrownByClient() throws Exception {
        when(jsonApiClient.callEnpointAndParse(anyString(), any())).thenThrow(new IOException("Something bad happened"));

        final List<Region> regions = client.retrieveAllRegions();
        assertTrue(regions.isEmpty());
    }

    //TODO writing other tests for the other 2 remaining endpoints is obviously a duplication. Same logic, just different method.
    // This points that the class under test could be refactored to get rid of the duplication.

}
