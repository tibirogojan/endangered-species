package com.tibi;

import com.tibi.model.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class EndangeredSpeciesFilterTest {

    private EndangeredSpeciesFilter filter;
    private IUCNRedListClient apiClient;

    @BeforeMethod
    public void setUp() {
        apiClient = mock(IUCNRedListClient.class);
        filter = new EndangeredSpeciesFilter(apiClient, new Random());

        //Assume only one region is returned so Random will yield the same output
        Region regionOfEurope = new Region();
        regionOfEurope.setIdentifier("europe");
        regionOfEurope.setName("Europe");
        when(apiClient.retrieveAllRegions()).thenReturn(Arrays.asList(regionOfEurope));
    }

    @Test
    public void shouldBeEmptyIfNothingIsReturnedByTheApi() {
        when(apiClient.retrieveEndangeredSpecies(any(Region.class), anyInt())).thenReturn(Collections.emptyList());

        FilterResults results = filter.filterForEndangeredSpeciesAndMammals();
        final List<Species> criticallyEndangeredSpecies = results.getCriticallyEndangeredSpecies();
        final List<Species> mammals = results.getMammals();

        assertTrue(criticallyEndangeredSpecies.isEmpty());
        assertTrue(mammals.isEmpty());
    }

    @Test
    public void shouldFilterOutMammalsCorrectly() {
        Species lizard = createNewSpeciesOfClass(BiologicalClass.REPTILIA);
        Species eagle = createNewSpeciesOfClass(BiologicalClass.AVES);
        Species lion = createNewSpeciesOfClass(BiologicalClass.MAMMALIA);
        Species bear = createNewSpeciesOfClass(BiologicalClass.MAMMALIA);

        List<Species> allSpecies = Arrays.asList(lizard, eagle, lion, bear);
        when(apiClient.retrieveEndangeredSpecies(any(Region.class), anyInt())).thenReturn(allSpecies);

        FilterResults results = filter.filterForEndangeredSpeciesAndMammals();
        final List<Species> mammals = results.getMammals();

        Assert.assertFalse(mammals.isEmpty());
        assertEquals(mammals, Arrays.asList(lion, bear));
    }

    @Test
    public void mammalsListShouldBeEmptyIfNoMammalsAreReturnedInTheFirstPlace() {
        Species lizard = createNewSpeciesOfClass(BiologicalClass.REPTILIA);
        Species eagle = createNewSpeciesOfClass(BiologicalClass.AVES);

        List<Species> allSpecies = Arrays.asList(lizard, eagle);
        when(apiClient.retrieveEndangeredSpecies(any(Region.class), anyInt())).thenReturn(allSpecies);

        FilterResults results = filter.filterForEndangeredSpeciesAndMammals();
        final List<Species> mammals = results.getMammals();

        assertTrue(mammals.isEmpty());
    }

    @Test
    public void shouldRetrieveCriticallyEndangeredSpeciesWithCorrectConservationMeasurements() {
        Species lizard = createNewSpeciesOfClass(1225, BiologicalClass.REPTILIA, EndangeredCategory.LC);            // don't worry
        Species eagle = createNewSpeciesOfClass(19987, BiologicalClass.AVES, EndangeredCategory.CR);                // endangered
        Species saberToothTiger = createNewSpeciesOfClass(1449, BiologicalClass.MAMMALIA, EndangeredCategory.EX);   // Extinct; again don't worry
        Species bear = createNewSpeciesOfClass(1245, BiologicalClass.MAMMALIA, EndangeredCategory.CR);              // endangered

        List<Species> allSpecies = Arrays.asList(lizard, eagle, saberToothTiger, bear);
        when(apiClient.retrieveEndangeredSpecies(any(Region.class), anyInt())).thenReturn(allSpecies);

        final List<String> measuresForEagles = Collections.emptyList();
        when(apiClient.retrieveConservationMeasuresForSpecies(eagle.getId()))
                .thenReturn(createListMeasures(measuresForEagles));

        final List<String> measuresForBears = Arrays.asList("More honey", "Less hunting");
        when(apiClient.retrieveConservationMeasuresForSpecies(bear.getId())).thenReturn(createListMeasures(measuresForBears));

        FilterResults results = filter.filterForEndangeredSpeciesAndMammals();
        final List<Species> endangeredSpecies = results.getCriticallyEndangeredSpecies();

        Assert.assertFalse(endangeredSpecies.isEmpty());
        assertEquals(endangeredSpecies, Arrays.asList(eagle, bear));

        final Optional<Species> eagleInResults = endangeredSpecies.stream().filter(es -> es.equals(eagle)).findFirst();
        assertTrue(eagleInResults.isPresent());
        assertFalse(eagleInResults.get().getConservationMeasuresSummary().isPresent());

        final Optional<Species> bearInResults = endangeredSpecies.stream().filter(es -> es.equals(bear)).findFirst();
        assertTrue(bearInResults.isPresent());
        assertEquals(bearInResults.get().getConservationMeasuresSummary().get(), String.join(", ", measuresForBears));
    }

    private List<ConservationMeasure> createListMeasures(List<String> whatToDo) {
        return whatToDo.stream()
                .map(what -> {
                    final ConservationMeasure conservationMeasure = new ConservationMeasure();
                    conservationMeasure.setCode("1.1"); // not 100% following the real API.. but irrelevant here
                    conservationMeasure.setTitle(what);
                    return conservationMeasure;
                })
                .collect(Collectors.toList());
    }

    private Species createNewSpeciesOfClass(int id, BiologicalClass biologicalClass, EndangeredCategory category) {
        final Species species = new Species();
        species.setId(id);
        species.setBiologicalClass(biologicalClass);
        species.setEndangeredCategory(category);
        return species;
    }

    private Species createNewSpeciesOfClass(BiologicalClass biologicalClass) {
        final Species species = new Species();
        species.setBiologicalClass(biologicalClass);
        return species;
    }
}
